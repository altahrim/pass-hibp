##################################################
### Variables                                  ###
##################################################
# You can change following variables at runtime
# ie: PREFIX=/usr/local make install
PROG ?= hibp
PREFIX ?= /usr
LIBDIR ?= $(PREFIX)/lib
SYSTEM_EXTENSION_DIR ?= $(LIBDIR)/password-store/extensions
BASHCOMPDIR ?= /etc/bash_completion.d

##################################################
### Introduction / Help                        ###
##################################################
help:
	echo 'pass-$(PROG) is a shell script and does not need compilation, it can be simply executed.'
	echo
	echo 'To install it try "sudo make install" instead.'
	echo
	echo 'To run pass-$(PROG) one needs to have some tools installed on the system:'
	echo  ' – password store (http://passwordstore.org),'
	echo  ' – jq (https://stedolan.github.io/jq),'
	echo  ' – curl (https://curl.haxx.se).'


##################################################
### Tests                                      ###
##################################################
lint: shellcheck

shellcheck:
	shellcheck -a hibp.bash


##################################################
### Installation                               ###
##################################################
install:
	install -v -d '$(DESTDIR)$(SYSTEM_EXTENSION_DIR)/'
	install -v -m0755 hibp.bash '$(DESTDIR)$(SYSTEM_EXTENSION_DIR)/$(PROG).bash'
	install -v -d '$(DESTDIR)$(BASHCOMPDIR)/pass-$(PROG)'
	install -v -m0644 pass-hibp.bash.completion '$(DESTDIR)$(BASHCOMPDIR)/pass-$(PROG)/pass-$(PROG).bash.completion'
	echo
	echo "pass-$(PROG) is installed succesfully"

uninstall:
	rm -vrf '$(DESTDIR)$(SYSTEM_EXTENSION_DIR)/$(PROG).bash' '$(DESTDIR)$(BASHCOMPDIR)/pass-$(PROG)'
	if [[ -d '$(DESTDIR)$(SYSTEM_EXTENSION_DIR)' ]]; then \
		rmdir -v --ignore-fail-on-non-empty '$(DESTDIR)$(SYSTEM_EXTENSION_DIR)'; \
	fi
	echo
	echo "pass-$(PROG) is uninstalled succesfully"
	echo


##################################################
.SILENT: help install uninstall
.PHONY: lint shellcheck test install uninstall
