#!/usr/bin/env bash
set -e
set -u

declare -a PASS_ENTRIES=()
declare -r VERSION="dev"
declare -a ACCOUNTS_LIST=()
declare -A ACCOUNTS_POWNED=()
declare -a HASHES_LIST=()
declare -A HASHES_POWNED=()
declare -a WEBSITES_LIST=()
declare -A WEBSITES_POWNED=()

# Pass HIBP extension.
function main() {
  parse_opts "${@}"
  cmd_hibp_check
}

# Parse command line options.
function parse_opts() {
  declare GET_OPT
  GET_OPT="$(getopt -n pass-hibp -o hV --long help,version -- "${@}")"
  eval set -- "${GET_OPT}"

  while [[ "${#}" -gt 0 ]]; do
    if [[ "${1}" == '--' ]]; then
      shift
      break;
    fi

    case "${1}" in
      -h|--help)
        cmd_hibp_usage
        exit 0
        ;;
      -V|--version)
        cmd_hibp_version
        exit 0
        ;;
      *)
        echo -e "\e[31mInternal error!\e[0m"
        exit 1
        ;;
    esac

    shift
  done
}

# Check pass entries against HIBP API.
function cmd_hibp_check() {
  check_prerequisites

  echo -n "Loading entries from pass…"
  load_entries
  build_lists
  echo -e " \e[32;1mOK\e[0m"

  echo -n "Checking information against HIBP…"
  check_hashes
  check_accounts
  check_websites
  echo -e " \e[32;1mOK\e[0m"

  display_results
}

# Displays pass-hibp help
function cmd_hibp_usage() {
  echo "Usage of ${PROGRAM}"
}

# Displays pass-hibp version
function cmd_hibp_version() {
  echo "pass-hibp version ${VERSION}"
}

# Check extension dependencies
function check_prerequisites() {
  if ! command -v jq >& /dev/null; then
    echo 'Command "jq" is required for HIBP plugin.'
    exit 1
  fi
  if ! command -v curl >& /dev/null; then
    echo 'Command "curl" is required for HIBP plugin.'
    exit 1
  fi
}

# Copy values from ARG_LIST to keys in ARG_POWNED.
function prepare_list()
{
  declare -n FROM_LIST="${1}_LIST"
  declare -n TO_ARRAY="${1}_POWNED"
  for ENTRY in "${FROM_LIST[@]}"; do
    # shellcheck disable=SC2034
    TO_ARRAY["${ENTRY}"]=
  done
}

# Check each hash against HIBP API.
function check_hashes() {
  prepare_list HASHES
  for HASH in "${!HASHES_POWNED[@]}"; do
    while read -r LINE; do
      HASH_SUFFIX="$(<<< "${LINE}" cut -d: -f1)"
      if [[ "${HASH:5}" = "${HASH_SUFFIX}" ]]; then
        HASHES_POWNED[${HASH}]=true
        break
       fi
    done < <(pwnedpass_request "range/${HASH:0:5}")
  done
}

# Check each account against HIBP API.
function check_accounts() {
  prepare_list ACCOUNTS
  declare JSON_RESPONSE
  for ACCOUNT in "${!ACCOUNTS_POWNED[@]}"; do
    JSON_RESPONSE="$(hibp_request "breachedaccount/${ACCOUNT}")"
    if [[ "$(<<< "${JSON_RESPONSE}" jq '. | length')" -gt 0 ]]; then
      ACCOUNTS_POWNED["${ACCOUNT}"]="${JSON_RESPONSE}"
    fi
    sleep 1.7
  done
}

# Check each website against HIBP API.
function check_websites() {
  prepare_list WEBSITES
  declare JSON_RESPONSE
  for WEBSITE in "${!WEBSITES_POWNED[@]}"; do
    JSON_RESPONSE="$(hibp_request "breaches?domain=${WEBSITE}")"
    if [[ "$(<<< "${JSON_RESPONSE}" jq '. | length')" -eq 0 ]]; then
      continue
    fi
  done
}

# Send a request to pownedpasswords API.
function pwnedpass_request() {
  api_request api.pwnedpasswords.com "${@}"
}

# Send a request to hibp API.
function hibp_request() {
  api_request haveibeenpwned.com/api "${@}"
}

# Send a generic API request.
function api_request() {
  declare -r ENDPOINT="${1}"
  declare -r REQUEST="${2}"
  declare -r USER_AGENT="HIBPForPass ${VERSION}"
  declare -ri API_VERSION=2
  declare -r API_ROOT_URL="https://${ENDPOINT}"

  declare CODE OUTPUT OUTPUT_FILE
  OUTPUT_FILE="$(mktemp)"
  CODE=$(curl -s -w "%{http_code}" -o "${OUTPUT_FILE}" -A "${USER_AGENT}" -H "API-Version: ${API_VERSION}" "${API_ROOT_URL}/${REQUEST}")
  OUTPUT="$(cat "${OUTPUT_FILE}")"

  # Check API response
  # https://haveibeenpwned.com/API/v2#ResponseCodes
  case "${CODE}" in
    200)
      echo "${OUTPUT}"
      return 0
      ;;
    400)
      echo "400 Bad request"
      echo "The account does not comply with an acceptable format (i.e. it's an empty string)."
      return 1
      ;;
    403)
      echo "403 Forbidden"
      echo "No user agent has been specified in the request."
      return 2
      ;;
    404)
      echo "404 Not found"
      echo "The account could not be found and has therefore not been pwned."
      return 4
      ;;
    429)
      echo "429 Too many requests"
      echo "The rate limit has been exceeded."
      return 3
      ;;
    503)
      echo "503 Service Unavailable"
      return 4
      ;;
  esac
}

# Read all entries from password store.
function load_entries() {
  mapfile -t PASS_ENTRIES < <(find "${PREFIX}" -type f -iname '*.gpg' -printf "%P\n" | sed 's/.gpg$//')
}

# Build work list from password store entries
# Deciphering happens here.
function build_lists() {
  declare -i ENTRY_ID
  for ENTRY_ID in "${!PASS_ENTRIES[@]}"; do
    declare -n ENTRY="PASS_ENTRIES[${ENTRY_ID}]"
    CONTENT="$(pass show "${ENTRY}")"

    HASH="$(printf '%s' "$(<<< "${CONTENT}" head -n1)" | sha1sum | awk '{print $1}' | tr '[:lower:]' '[:upper:]' )"
    if WEBSITE="$(<<< "${CONTENT}" grep -i 'url:')"; then
      WEBSITE="$(<<< "${WEBSITE}" awk '{print $2}' | cut -d/ -f3 | tr '[:upper:]' '[:lower:]')"
    fi
    if ACCOUNT="$(<<< "${CONTENT}" grep -E '^(login|email)')"; then
      ACCOUNT="$(<<< "${ACCOUNT}" awk '{print $2}' | tr '[:upper:]' '[:lower:]')"
    fi
    HASHES_LIST["${ENTRY_ID}"]="${HASH}"
    if [[ -n "${ACCOUNT}" ]]; then
      ACCOUNTS_LIST["${ENTRY_ID}"]="${ACCOUNT}"
    fi
    if [[ -n "${WEBSITE}" ]]; then
      WEBSITES_LIST["${ENTRY_ID}"]="${WEBSITE}"
    fi
  done
}

# Display results
function display_results() {
  declare ENTRY ACCOUNT HASH WEBSITE POWNED=false
  declare -a ALERTS
  for ENTRY_ID in "${!PASS_ENTRIES[@]}"; do
    ENTRY="${PASS_ENTRIES[${ENTRY_ID}]}"
    ACCOUNT="${ACCOUNTS_LIST[${ENTRY_ID}]:-}"
    HASH="${HASHES_LIST[${ENTRY_ID}]}"
    WEBSITE=${WEBSITES_LIST[${ENTRY_ID}]:-}
    ALERTS=()

    if [[ -n "${HASHES_POWNED[${HASH}]}" ]]; then
      ALERTS=('This password has been powned! You should change it as soon as possible.')
    fi
    if [[ -n "${WEBSITE}" ]] && [[ -n "${WEBSITES_POWNED[${WEBSITE}]}" ]]; then
      ALERTS=("A breach happened on ${WEBSITE}.")
    fi
    if [[ -n "${ACCOUNT}" ]] && [[ -n "${ACCOUNTS_POWNED[${ACCOUNT}]}" ]]; then
      ALERTS=("The account \"${ACCOUNT}\" already appeared in a breach.")
    fi

    if [[ -n "${ALERTS[*]}" ]]; then
      POWNED=true
      printf 'Entry \e[1m%s\e[0m is probably compromised:\n' "${ENTRY}"
      for ALERT in "${ALERTS[@]}"; do
        printf ' – %s\n' "${ALERT}"
      done
    fi
  done

  if ! ${POWNED}; then
    echo -e "\e[32mWell done! None of your account appear in Have I Been Powned.\e[0m"
  fi
}

main "${@}"
