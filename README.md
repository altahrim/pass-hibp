# pass-hibp
Check your pass entries against [Have I Been Powned (HIBP)](http://haveibeenpwned.com/) API.

It allows you to know if some of your accounts have been compromised in data breaches.

## Requirements
 * `pass` (http://passwordstore.org),
 * `jq` (https://stedolan.github.io/jq),
 * `curl` (https://curl.haxx.se).

## Installation
### From Git
```bash
git clone https://gitlab.com/altahrim/pass-hibp.git
cd pass-hibp
sudo make install
```

## Usage
 ```bash
 pass hibp
 ```
